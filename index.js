module.exports = {
  root: true,
  parser: 'vue-eslint-parser',
  parserOptions: {
    parser: 'babel-eslint'
  },
  env: {
    node: true
  },
  plugins: [
    'import',
    'vue'
  ],
  extends: [
    'plugin:vue/recommended',
    '@vue/standard'
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'vue/prop-name-casing': 'off',
    'camelcase': 'off',
    'comma-dangle': 'off',
    'semi': ['warn', 'always'],
    'no-unused-vars': 'warn',
    'import/order': ['warn', {
      'newlines-between': 'always',
      'alphabetize': {
        'order': 'desc',
        'caseInsensitive': true
      }
    }]
  }
}